CLASS zcl_test_inherit_class DEFINITION
  PUBLIC
  INHERITING FROM zcl_test_root_class
  CREATE PUBLIC .

  PUBLIC SECTION.
  PROTECTED SECTION.
    METHODS protected
        REDEFINITION.

    METHODS example_01
        REDEFINITION .
  PRIVATE SECTION.
ENDCLASS.



CLASS ZCL_TEST_INHERIT_CLASS IMPLEMENTATION.


  METHOD example_01.
    write:/ 'child::example_01'.
  ENDMETHOD.


  METHOD protected.
    write:/ 'child::protected'.
  ENDMETHOD.
ENDCLASS.
