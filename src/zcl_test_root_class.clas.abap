CLASS zcl_test_root_class DEFINITION
  PUBLIC
  ABSTRACT
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS public.

  PROTECTED SECTION.
    METHODS protected.

    METHODS example_01 .
    METHODS example_02 .

  PRIVATE SECTION.
    METHODS private.

ENDCLASS.



CLASS ZCL_TEST_ROOT_CLASS IMPLEMENTATION.


  METHOD example_01.
    write: / 'root::example_01'.
  ENDMETHOD.


  METHOD example_02.
    write: / 'root::example_02'.
  ENDMETHOD.


  METHOD private.

  ENDMETHOD.


  METHOD protected.
    write: / 'root::protected'.
  ENDMETHOD.


  METHOD public.
    me->example_01( ).
    me->protected( ).
  ENDMETHOD.
ENDCLASS.
